<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Dumper\Loop;

use Dumper\Dumper;
use Dumper\Export\Formatters\DumperCompressor;
use Thelia\Core\Template\Loop\ArchiveBuilder;

/**
 * Description of CompressOptionsLoop
 *
 * @author Romain
 */
class CompressOptionsLoop extends ArchiveBuilder
{
    //put your code here
    
    public function buildArray()
    {
        $opts = parent::buildArray();
        $dumperCompressors = $this->container->get(Dumper::DCM_SERVICE_ID)->getDumperCompressors();
        
        foreach ($dumperCompressors as $dfid => $dumperCompressor) {
            if ($dumperCompressor instanceof DumperCompressor
                && $dumperCompressor->isAvailable()
            ) {
                $opts[$dfid] = $dumperCompressor;
            }
        }

        switch ($this->getOrder()) {
            case "alpha":
                ksort($opts);
                break;
            case "alpha_reverse":
                krsort($opts);
                break;
        }

        return $opts;
    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Dumper\Loop;

use Dumper\Handler\DumperHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Thelia\Core\Template\Element\ArraySearchLoopInterface;
use Thelia\Core\Template\Element\BaseLoop;
use Thelia\Core\Template\Element\LoopResult;
use Thelia\Core\Template\Element\LoopResultRow;
use Thelia\Core\Template\Loop\Argument\Argument;
use Thelia\Core\Template\Loop\Argument\ArgumentCollection;

/**
 * Description of DatabasesLoop
 *
 * @author Romain
 */
class DatabasesLoop extends BaseLoop implements ArraySearchLoopInterface
{
    /** @var DumperHandler */
    protected $dumperHandler;
    
    const DBNAME = 'DBNAME';

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->dumperHandler = $container->get('dumper.handler');
    }
    
    
    protected function getArgDefinitions()
    {
        return new ArgumentCollection(
            Argument::createBooleanTypeArgument('current_env')
        );
    }

    public function buildArray()
    {
        $results = [];
        
        $results[] = ['dbname' => $this->dumperHandler->getDbName()];
        
        return $results;
    }

    public function parseResults(LoopResult $loopResult)
    {
        /** @var array $dbRow **/
        foreach ($loopResult->getResultDataCollection() as $dbRow) {
            $row = (new LoopResultRow())->set(static::DBNAME, $dbRow['dbname']);
            $loopResult->addRow($row);
        }
        
        return $loopResult;
    }
}

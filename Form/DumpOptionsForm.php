<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Dumper\Form;

use Dumper\Dumper;
use Dumper\Handler\DumperHandler;
use Ifsnop\Mysqldump\Mysqldump;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\ExecutionContextInterface;
use Thelia\Core\HttpFoundation\Request;
use Thelia\Core\Template\Element\LoopResultRow;
use Thelia\Form\BaseForm;

/**
 * Description of DumpOptionsForm
 *
 * @author Romain
 */
class DumpOptionsForm extends BaseForm
{
    const FORM_NAME = 'dumper_dump_opts';
    const DBNAME_FIELD = 'dbname';
    const DUMP_FIELDS = 'dump_settings';
    const PDO_FIELDS = 'pdo_settings';
    
    const ROOT_TRANSLATION_KEY = 'dump_form.advanced params.dump.';
    
    // Dump settings field names
    
    /** @var string */
    const DUMP_INCLUDE_TABLES = 'include-tables';
    
    /** @var string */
    const DUMP_EXCLUDE_TABLES = 'exclude-tables';
    
    /** @var string */
    const DUMP_COMPRESS = 'compress';
    
    /** @var string */
    const DUMP_NO_DATA = 'no-data';
    
    /** @var string */
    const DUMP_ADD_DROP_TABLE = 'add-drop-table';
    
    /** @var string */
    const DUMP_SINGLE_TRABSACTION = 'single-transaction';
    
    /** @var string */
    const DUMP_LOCK_TABLES = 'lock-tables';
    
    /** @var string */
    const DUMP_ADD_LOCKS = 'add-locks';
    
    /** @var string */
    const DUMP_EXTENDED_INSERT = 'extended-insert';
    
    /** @var string */
    const DUMP_DISABLE_KEYS = 'disable-keys';
    
    /** @var string */
    const DUMP_WHERE = 'where';
    
    /** @var string */
    const DUMP_NO_CREATE_INFO = 'no-create-info';
    
    /** @var string */
    const DUMP_SKIP_TRIGGERS = 'skip-triggers';
    
    /** @var string */
    const DUMP_ADD_DROP_TRIGGER = 'add-drop-trigger';
    
    /** @var string */
    const DUMP_HEX_BLOB = 'hex-blob';
    
    /** @var string */
    const DUMP_ADD_DROP_DATABASE = 'add-drop-database';
    
    /** @var string */
    const DUMP_SKIP_TZ_UTC = 'skip-tz-utc';
    
    /** @var string */
    const DUMP_NO_AUTOCOMMIT = 'no-autocommit';
    
    /** @var string */
    const DUMP_DEFAULT_CHARACTER_SET = 'default-character-set';
    
    /** @var string */
    const DUMP_SKIP_COMMENTS = 'skip-comments';
    
    /** @var string */
    const DUMP_SKIP_DUMP_DATE = 'skip-dump-date';
    
    /** @var DumperHandler */
    protected $dumperHandler;
    
    /** @var array */
    protected $databases;

    /**
     * Overriden constructor, for DumpOptionsForm specific attributes
     * @param Request $request
     * @param type $type
     * @param type $data
     * @param type $options
     * @param ContainerInterface $container
     */
    public function __construct(
        Request $request,
        $type = "form",
        $data = array(),
        $options = array(),
        ContainerInterface $container = null
    ) {
        if ($container != null) {
            $this->dumperHandler = $container->get('dumper.handler');
            $this->databases = $this->dumperHandler->getDatabases($container);
        } else {
            $this->databases = [];
        }
        
        parent::__construct($request, $type, $data, $options, $container);
    }
    
    public function getName()
    {
        return static::FORM_NAME;
    }
    
    protected function buildForm()
    {
        $this->formBuilder
            ->add(self::DBNAME_FIELD, 'choice', [
                'multiple' => false,
                'expanded' => true,
                'required' => true,
                'label' => $this->trans('dump_form.header'),
                'label_attr' => [
                    'for' => self::DBNAME_FIELD,
                ],
                'choices' => array_combine($this->databases, $this->databases),
                'data' => $this->dumperHandler->getDbName(),
                
            ])
            ->add(
                $this->formBuilder->create(self::DUMP_FIELDS, 'form', [])
                    ->add(self::DUMP_INCLUDE_TABLES, 'choice', [
                        'multiple' => false,
                        'expanded' => false,
                        'required' => true,
                        'label' => $this->trans(self::ROOT_TRANSLATION_KEY.self::DUMP_INCLUDE_TABLES),
                        'label_attr' => [
                            'for' => self::DUMP_INCLUDE_TABLES,
                        ],
                        'constraints' => new Callback(['methods' => [
                            [$this, 'belongToDatabase']
                        ]])
                    ])
                    ->add(self::DUMP_EXCLUDE_TABLES, 'choice', [
                        'multiple' => false,
                        'expanded' => false,
                        'required' => true,
                        'label' => $this->trans(self::ROOT_TRANSLATION_KEY.self::DUMP_EXCLUDE_TABLES),
                        'label_attr' => [
                            'for' => self::DUMP_EXCLUDE_TABLES,
                        ],
                        'constraints' => new Callback(['methods' => [
                            [$this, 'belongToDatabase']
                        ]])
                    ])
                    ->add(self::DUMP_COMPRESS, 'choice', [
                        'multiple' => false,
                        'expanded' => false,
                        'required' => true,
                        'label' => $this->trans(sprintf(
                            self::ROOT_TRANSLATION_KEY."%s.label",
                            self::DUMP_COMPRESS
                        )),
                        'label_attr' => [
                            'for' => self::DUMP_COMPRESS,
                        ],
                        'choices' => $this->getCompressOptions(),
                        'empty_data' => Mysqldump::NONE,
                    ])
                    ->add($this->createCheckboxDumpField(self::DUMP_NO_DATA, 0))
                    ->add($this->createCheckboxDumpField(self::DUMP_ADD_DROP_TABLE, 0))
                    ->add($this->createCheckboxDumpField(self::DUMP_SINGLE_TRABSACTION, 0))
                    ->add($this->createCheckboxDumpField(self::DUMP_LOCK_TABLES, 0))
                    ->add($this->createCheckboxDumpField(self::DUMP_ADD_LOCKS, 1))
                    ->add($this->createCheckboxDumpField(self::DUMP_EXTENDED_INSERT, 1))
                    ->add($this->createCheckboxDumpField(self::DUMP_DISABLE_KEYS, 1))
                    ->add(self::DUMP_WHERE, 'text', [
                        // TODO: be careful to XSS at submit.
                        'required' => true,
                        'label' => $this->trans(self::ROOT_TRANSLATION_KEY.self::DUMP_WHERE),
                        'label_attr' => [
                            'for' => self::DUMP_WHERE,
                        ],
                        'data' => '',
                    ])
                    ->add($this->createCheckboxDumpField(self::DUMP_NO_CREATE_INFO, 0))
                    ->add($this->createCheckboxDumpField(self::DUMP_SKIP_TRIGGERS, 0))
                    ->add($this->createCheckboxDumpField(self::DUMP_ADD_DROP_TRIGGER, 1))
                    ->add($this->createCheckboxDumpField(self::DUMP_HEX_BLOB, 1))
                    ->add($this->createCheckboxDumpField(self::DUMP_ADD_DROP_DATABASE, 0))
                    ->add($this->createCheckboxDumpField(self::DUMP_SKIP_TZ_UTC, 0))
                    ->add($this->createCheckboxDumpField(self::DUMP_NO_AUTOCOMMIT, 1))
                    ->add(self::DUMP_DEFAULT_CHARACTER_SET, 'choice', [
                        'multiple' => false,
                        'expanded' => false,
                        'required' => true,
                        'label' => $this->trans(sprintf(
                            self::ROOT_TRANSLATION_KEY."%s.label",
                            self::DUMP_DEFAULT_CHARACTER_SET
                        )),
                        'label_attr' => [
                            'for' => self::DUMP_DEFAULT_CHARACTER_SET,
                        ],
                        'choices' => [
                            Mysqldump::UTF8 => $this->trans(sprintf(
                                self::ROOT_TRANSLATION_KEY."%s.%s",
                                self::DUMP_DEFAULT_CHARACTER_SET,
                                Mysqldump::UTF8
                            )),
                            Mysqldump::UTF8MB4 => $this->trans(sprintf(
                                self::ROOT_TRANSLATION_KEY."%s.%s",
                                self::DUMP_DEFAULT_CHARACTER_SET,
                                Mysqldump::UTF8MB4
                            )),
                        ],
                        'empty_data' => Mysqldump::UTF8,
                    ])
                    ->add($this->createCheckboxDumpField(self::DUMP_SKIP_COMMENTS, 0))
                    ->add($this->createCheckboxDumpField(self::DUMP_SKIP_DUMP_DATE, 0))
            )   // End dump settings
            ->add($this->formBuilder->create(self::PDO_FIELDS, 'form', []))
        ;
    }
    
    public function belongToDatabase($value, ExecutionContextInterface $context)
    {
        $data = $context->getRoot()->getData();
        $tables = $this->dumperHandler->getDatabaseTables($data[self::DBNAME_FIELD]);
        $outsiderTables = array_diff($value, $tables);
        
        if (!empty($outsiderTables)) {
            $context->addViolation($this->trans(
                'dump_form.advanced params.dump.tables error %dbname %tables',
                [
                    '%dbname' => $data[self::DBNAME_FIELD],
                    '%tables' => implode(', ', $outsiderTables),
                ]
            ));
        }
    }
    
    protected function trans(
        $key,
        array $parameters = [],
        $domain = Dumper::BO_DOMAIN_NAME
    ) {
        return $this->translator->trans($key, $parameters, $domain);
    }
    
    protected function getCompressOptions()
    {
        $pagination = null;
        $loop = new \Dumper\Loop\CompressOptionsLoop($this->container);
        $loop->initializeArgs(['order' => 'alpha']);
        $results = $loop->exec($pagination);
        $results->rewind();
        
        $options = [
            Mysqldump::NONE => $this->trans(sprintf(
                "dump_form.advanced params.dump.%s.none",
                self::DUMP_COMPRESS
            )),
        ];
        
        /** @var LoopResultRow $resrow */
        foreach ($results as $resrow) {
            $name = $resrow->get('NAME');
            $extension = $resrow->get('EXTENSION');
            $options[$name] = "$name (.$extension)";
        }
        
        return $options;
    }
    
    protected function createCheckboxDumpField($fieldName, $value)
    {
        return $this->formBuilder->create($fieldName, 'checkbox', [
            'required' => true,
            'label' => $this->trans(self::ROOT_TRANSLATION_KEY.$fieldName),
            'label_attr' => [
                'for' => $fieldName,
            ],
            'value' => $value,
        ]);
    }
}

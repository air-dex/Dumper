<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Dumper\Hook;

use Dumper\Dumper;
use Dumper\Handler\DumperHandler;
use Thelia\Core\Event\Hook\HookRenderEvent;
use Thelia\Core\Hook\BaseHook;
use Thelia\Tools\URL;

/**
 * Description of ConfigurationHook
 *
 * @author Romain
 */
class ConfigurationHook extends BaseHook
{
    /** @var string Thelia's database name */
    protected $theliaDb;

    /** @var URL Thelia's database name */
    protected $urlManager;

    /**
     * Class constructor
     *
     * @param DumperHandler $handler
     */
    public function __construct(DumperHandler $handler, URL $urlManager)
    {
        $this->theliaDb = $handler->getDbName();
        $this->urlManager = $urlManager;
    }
    
    public function onMainHeadCss(HookRenderEvent $event)
    {
        $event->add($this->addCSS('assets/css/bootstrap-multiselect.css'));
    }

    public function onModuleConfiguration(HookRenderEvent $event)
    {
        $event->add($this->render('module_configuration.html', [
            'THELIA_DB' => $this->theliaDb,
        ]));
    }
    
    public function onModuleConfigJs(HookRenderEvent $event)
    {
        $event->add($this->addJS('assets/js/bootstrap-multiselect.js'));
        $event->add($this->addJS('assets/js/dumper.js', [
            'id' => 'dumper-script',
            'data-tm-non-selected-text' => $this->transTM('nonSelectedText'),
            'data-tm-n-selected-text' => $this->transTM('nSelectedText'),
            'data-tm-select-all-text' => $this->transTM('selectAllText'),
            'data-tm-filter-placeholder' => $this->transTM('filterPlaceholder'),
            'data-get-tables-url' => $this->urlManager->absoluteUrl('/admin/module/Dumper/tables/%dbname'),
        ]));
    }
    
    protected function transTM($tmKey)
    {
        return $this->trans(
            "dump_form.advanced params.dump.tables-multiselect.$tmKey",
            [],
            Dumper::BO_DOMAIN_NAME
        );
    }
}

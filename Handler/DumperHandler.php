<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Dumper\Handler;

use Dumper\Export\Dump;
use Dumper\Export\Formatters\SQLFormatter;
use Dumper\Loop\DatabasesLoop;
use Dumper\Tools\Propeller;
use Ifsnop\Mysqldump\Mysqldump;
use PDO;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Propel;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Thelia\Core\Event\ImportExport;
use Thelia\Core\Event\TheliaEvents;
use Thelia\Core\FileFormat\Archive\AbstractArchiveBuilder;
use Thelia\Core\FileFormat\Archive\ArchiveBuilderInterface;
use Thelia\Core\FileFormat\Archive\ArchiveBuilderManager;
use Thelia\Core\FileFormat\Formatting\AbstractFormatter;
use Thelia\Core\HttpFoundation\Response;
use Thelia\Core\Template\Element\LoopResultRow;
use Thelia\ImportExport\Export\ExportHandler;
use Thelia\Model\Lang;

/**
 * Description of DumperHandler
 *
 * @author Romain
 */
class DumperHandler
{
    protected $propelConfig;
    
    protected $dsnArray;
    
    protected $environment;
    
    /** @var EventDispatcherInterface */
    protected $dispatcher;

    public function __construct($env, $debug, EventDispatcherInterface $dispatcher)
    {
        $handler = new Propeller($env, $debug);
        $this->propelConfig = $handler->getPropelConfig();
        $this->dsnArray = $this->parseDsn($this->propelConfig['dsn']);
        $this->environment = $env;
        $this->dispatcher = $dispatcher;
    }

    /**
     * Parse a DSN
     * @param string $dsn The DSN
     * @return array Parameters contained in the DSN
     */
    public function parseDsn($dsn)
    {
        $dsnParams = explode('mysql:', $dsn, 2)[1];
        
        $dsnArray = [];
        foreach (explode(';', $dsnParams) as $dsnParam) {
            $parsed = explode('=', $dsnParam, 2);
            $dsnArray[$parsed[0]] = $parsed[1];
        }
        
        return $dsnArray;
    }
    
    public function getDbName()
    {
        return $this->dsnArray['dbname'];
    }
    
    public function dumpDatabase(
        ContainerInterface $container,
        $dbName,
        $filename,
        array $dumpParams = [],
        array $pdoParams = []
    ) {
        if ($dbName == $this->getDbName()) {
            $dsn = $this->propelConfig['dsn'];
        } else {
            return '';
        }
        
        try {
            $dump = new Mysqldump(
                $dsn,
                $this->propelConfig['user'],
                $this->propelConfig['password'],
                $dumpParams,
                $pdoParams
            );

            $dump->start($filename);
            return file_get_contents($filename);
        } catch (\Exception $ex) {
            $container->get('thelia.logger').error(sprintf(
                "[Dumper] Cannot dump the '%s' database: %s",
                $dbName,
                $ex.getMessage()
            ));
            return '';
        }
    }
    
    /**
     * Getting databases that can be dumped by the module
     * @param ContainerInterface $container Container required for the
     * method execution.
     * @return array Array with databases names
     */
    public function getDatabases(ContainerInterface $container)
    {
        // Searching databases names
        $pagination = null;
        $loop = new DatabasesLoop($container);
        $loop->initializeArgs([
            'limit' => PHP_INT_MAX,
        ]);
        $results = $loop->exec($pagination);
        $results->rewind();
        
        // Building results array
        $res = [];
        
        /** @var LoopResultRow $resrow */
        foreach ($results as $resrow) {
            $res[] = $resrow->get(DatabasesLoop::DBNAME);
        }
        
        return $res;
    }
    
    public function exportDump(
        ContainerInterface $container,
        Lang $lang,
        $dbName,
        array $dumpSettings = [],
        array $pdoSettings = []
    ) {
        /**
         * Get needed services
         *
         * @var ArchiveBuilderManager $archiveBuilderManager
         */
        $archiveBuilderManager = $container->get('thelia.manager.archive_builder_manager');

        // Get the archive builder if we have to compress the file(s)

        // TODO: Implement with advanced configuration of compressing
        $archiveBuilder = null;
        if (false) {
            /** @var ArchiveBuilderInterface $archiveBuilder */
            /*$archiveBuilder = $archiveBuilderManager->get(
                $boundForm->get("archive_builder")->getData()
            );*/
        }

        // Return the generated Response
        return $this->processExport(
            new SQLFormatter(),
            new Dump($container, $dbName, $dumpSettings, $pdoSettings),
            $archiveBuilder,
            $lang
        );
    }

    /**
     * @param AbstractFormatter $formatter
     * @param ExportHandler $handler
     * @param AbstractArchiveBuilder $archiveBuilder
     * @param Lang $lang
     * @param bool $includeImages
     * @param bool $includeDocuments
     * @return Response
     *
     * Processes an export by returning a response with the export's content.
     */
    protected function processExport(
        AbstractFormatter $formatter,
        ExportHandler $handler,
        AbstractArchiveBuilder $archiveBuilder = null,
        Lang $lang = null
    ) {
        /**
         * Build an event containing the formatter and the handler.
         * Used for specific configuration (e.g: XML node names)
         */

        $event = new ImportExport($formatter, $handler);
        $event->setDispatcher($this->dispatcher);

        $filename = $handler->getFilename() . "." . $formatter->getExtension();

        if ($archiveBuilder === null) {
            $data = $handler->buildData($lang);

            $event->setData($data);
            $this->dispatcher->dispatch(
                TheliaEvents::EXPORT_BEFORE_ENCODE,
                $event
            );

            $formattedContent = $formatter
                ->setOrder($handler->getOrder())
                ->encode($data)
            ;

            $this->dispatcher->dispatch(
                TheliaEvents::EXPORT_AFTER_ENCODE,
                $event->setContent($formattedContent)
            );

            return new Response(
                $event->getContent(),
                200,
                [
                    'Content-Type' => $formatter->getMimeType(),
                    'Content-Disposition' => "attachment; filename=\"$filename\"",
                ]
            );
        } else {
            $event->setArchiveBuilder($archiveBuilder);

            $data = $handler
                ->buildData($lang)
                ->setLang($lang)
            ;

            $this->dispatcher->dispatch(
                TheliaEvents::EXPORT_BEFORE_ENCODE,
                $event
            );

            $formattedContent = $formatter
                ->setOrder($handler->getOrder())
                ->encode($data)
            ;

            $this->dispatcher->dispatch(
                TheliaEvents::EXPORT_AFTER_ENCODE,
                $event->setContent($formattedContent)
            );


            $archiveBuilder->addFileFromString(
                $event->getContent(),
                $filename
            );

            return $archiveBuilder->buildArchiveResponse(
                $handler->getFilename()
            );
        }
    }
    
    public function getDatabaseTables($dbName)
    {
        return $this->getTablesFromConnection($this->getDbConnection($dbName));
    }
    
    protected function getDbConnection($dbName)
    {
        if ($dbName == $this->getDbName()) {
            return Propel::getConnection();
        } else {
            return null;
        }
    }
    
    protected function getTablesFromConnection(ConnectionInterface $connection)
    {
        $tables = [];
        $result = $connection->prepare('SHOW TABLES');
        $result->execute();
        while ($row = $result->fetch(PDO::FETCH_NUM)) {
            $tables[] = $row[0];
        }
        
        return $tables;
    }
}

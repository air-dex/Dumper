<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Dumper\Handler;

use Dumper\Export\Formatters\DumperCompressor;

/**
 * Description of DumperCompressManager
 *
 * @author Romain
 */
class DumperCompressManager
{
    protected $dumperCompressors;
    
    public function __construct()
    {
        $this->dumperCompressors = [];
    }
    
    public function addDumperCompressor(DumperCompressor $dumperCompress)
    {
        $this->dumperCompressors[$dumperCompress->getName()] = $dumperCompress;
    }
    
    public function getDumperCompressors()
    {
        return $this->dumperCompressors;
    }
}

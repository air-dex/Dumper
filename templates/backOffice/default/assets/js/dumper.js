$(document).ready(function () {
    var $scriptTag = $('script#dumper-script');    // Script parameters
    var $tablesSelect = $('.tables-select');
    var $getTablesUrl = $scriptTag.attr('data-get-tables-url');
    
    $tablesSelect.multiselect({
        nonSelectedText: $scriptTag.attr('data-tm-non-selected-text'),
        numberDisplayed: 10,
        nSelectedText: $scriptTag.attr('data-tm-n-selected-text'),
        includeSelectAllOption: true,
        selectAllText: $scriptTag.attr('data-tm-select-all-text'),
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        filterPlaceholder: $scriptTag.attr('data-tm-filter-placeholder')
    });
    
    // Reloading tables when the database is changed
    function loadTables() {
        $.getJSON(
            $getTablesUrl.replace('%dbname', $('input.db-radio:checked').val()),
            {},
            function(data) {
                $tablesSelect.html('');
                $.each(data, function (key, tableName) {
                    var $option = $('<option>');
                    $option.attr('value', tableName);
                    $option.html(tableName);
                    $tablesSelect.append($option);
                });
                $tablesSelect.multiselect('rebuild');
            }
        ).fail(function() {
            $tablesSelect.html('');
            $tablesSelect.multiselect('rebuild');
        });
    }
    
    $('input.db-radio:checked').change(loadTables);
    loadTables();
});

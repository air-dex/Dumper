<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Dumper\Controller;

use Dumper\Handler\DumperHandler;
use Thelia\Controller\Admin\ExportController;
use Thelia\Core\HttpFoundation\JsonResponse;
use Thelia\Form\Exception\FormValidationException;

/**
 * Description of DumperController
 *
 * @author Romain
 */
class DumperController extends ExportController
{
    /**
     * @return DumperHandler
     */
    public function getDumperHandler()
    {
        return $this->container->get('dumper.handler');
    }
    
    public function processDumpAction()
    {
        if (!$this->getSecurityContext()->hasAdminUser()) {
            $this->accessDenied();
        }
        
        $form = $this->createForm('dumper.dump_opts');
        
        try {
            $data = $this->validateForm($form)->getData();
            
            return $this->getDumperHandler()->exportDump(
                $this->container,
                $this->getSession()->getAdminEditionLang(),
                $data['dbname']
            );
        } catch (FormValidationException $ex) {
            $message = $this->createStandardFormValidationErrorMessage($ex);
            $form->setErrorMessage($message);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
        }
        
        if (!isset($message)) {
            $this->pageNotFound();
        }
        
        $this->getParserContext()
            ->setGeneralError($message)
            ->addForm($form)
        ;
        
        $this->container->get('thelia.logger')->error(sprintf(
            "[Dumper] An error occured while performing a SQL dump: %s.",
            $message
        ));
        
        return $this->generateRedirect('/admin/module/Dumper');
    }
    
    public function getTablesAction($dbName)
    {
        $this->checkXmlHttpRequest();
        
        if (!$this->getSecurityContext()->hasAdminUser()) {
            return JsonResponse::createAuthError(null);
        }
        
        $tables = $this->getDumperHandler()->getDatabaseTables($dbName);
        return JsonResponse::create($tables);
    }
}

<?php
/*************************************************************************************/
/*      This file is part of the Thelia package.                                     */
/*                                                                                   */
/*      Copyright (c) OpenStudio                                                     */
/*      email : dev@thelia.net                                                       */
/*      web : http://www.thelia.net                                                  */
/*                                                                                   */
/*      For the full copyright and license information, please view the LICENSE.txt  */
/*      file that was distributed with this source code.                             */
/*************************************************************************************/

namespace Dumper;

use Thelia\Module\BaseModule;

class Dumper extends BaseModule
{
    /** @var string i18n Domain */
    const DOMAIN_NAME = 'dumper';
    
    /** @var string BackOffice i18n Domain */
    const BO_DOMAIN_NAME = 'dumper.bo.default';
    
    /** @var string Export handled type */
    const DUMPER_EXPORT = 'dumper.export.sql';
    
    const DUMPER_FORMATTER_TAG = 'dumper.compressor';
    
    const DCM_SERVICE_ID = 'dumper.compress.manager';
    
    public static function getCompilers()
    {
        return [
            new DependencyInjection\Compiler\DumperCompressorCompiler(),
        ];
    }
}

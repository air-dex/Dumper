<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Dumper\Export\Formatters;

/**
 * Description of DumperFormatter
 *
 * @author Romain
 */
interface DumperCompressor extends \Thelia\Core\FileFormat\FormatInterface
{
    public function isAvailable();
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Dumper\Export\Formatters;

use Dumper\Dumper;
use Thelia\Core\FileFormat\Formatting\AbstractFormatter;
use Thelia\Core\FileFormat\Formatting\FormatterData;

/**
 * Description of SQLFormatter
 *
 * @author Romain
 */
class SQLFormatter extends AbstractFormatter
{
    public function getName()
    {
        return 'SQL';
    }
    
    public function getExtension()
    {
        return 'sql';
    }

    public function getHandledType()
    {
        return Dumper::DUMPER_EXPORT;
    }

    public function getMimeType()
    {
        return 'application/sql';
    }

    // Decode / encode functions. Not really important here.
    
    public function decode($rawData)
    {
        return (new FormatterData())->setData([$rawData]);
    }

    public function encode(FormatterData $data)
    {
        return $data->getData()[0];
    }
}

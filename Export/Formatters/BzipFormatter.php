<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Dumper\Export\Formatters;

use Ifsnop\Mysqldump\Mysqldump;

/**
 * Description of BzipFormatter
 *
 * @author Romain
 */
class BzipFormatter extends SQLFormatter implements DumperCompressor
{
    public function getName()
    {
        return Mysqldump::BZIP2;
    }
    
    public function getExtension()
    {
        return 'sql.bz2';
    }

    public function getMimeType()
    {
        return 'application/x-bzip';
    }

    public function isAvailable()
    {
        return extension_loaded("bz2");
    }
}

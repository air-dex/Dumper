<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Dumper\Export\Formatters;

use Ifsnop\Mysqldump\Mysqldump;

/**
 * Description of GzipFormatter
 *
 * @author Romain
 */
class GzipFormatter extends SQLFormatter implements DumperCompressor
{
    public function getName()
    {
        return Mysqldump::GZIP;
    }
    
    public function getExtension()
    {
        return 'sql.gz';
    }

    public function getMimeType()
    {
        return 'application/x-gzip';
    }

    public function isAvailable()
    {
        return extension_loaded("zlib");
    }
}

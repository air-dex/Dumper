<?php
namespace Dumper\Export;

use Dumper\Dumper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Thelia\ImportExport\Export\ExportHandler;
use Thelia\Model\Admin;
use Thelia\Model\Lang;

/**
 * Description of Dump
 *
 * @author Romain
 */
class Dump extends ExportHandler
{
    protected $dbName;
    
    /** @var \Dumper\Handler\DumperHandler */
    protected $handler;

    /** @var array */
    protected $dumpParams;

    /** @var array */
    protected $pdoParams;
    
    public function __construct(
        ContainerInterface $container,
        $dbName = null,
        array $dumpParams = [],
        array $pdoParams = []
    ) {
        parent::__construct($container);
        $this->handler = $container->get('dumper.handler');
        $this->dbName = $dbName == null ? $this->handler->getDbName() : $dbName;
        $this->dumpParams = $dumpParams;
        $this->pdoParams = $pdoParams;
    }

    public function getHandledTypes()
    {
        return Dumper::DUMPER_EXPORT;
    }
    
    /**
     * Performs the dump
     * @param Lang $lang
     */
    public function buildDataSet(Lang $lang)
    {
        $tmpFile = tempnam(
            THELIA_CACHE_DIR.$this->container->getParameter('kernel.environment').DS,
            $this->getFilename()
        );
        $sql = $this->handler->dumpDatabase(
            $this->container,
            $this->dbName,
            $tmpFile,
            $this->dumpParams
        );
        unlink($tmpFile);
        
        // Log who tries to perform a dump
        /** @var Admin $admin */
        $admin = $this->container->get('thelia.securityContext')->getAdminUser();
        $this->container->get('thelia.logger')->info(sprintf(
            "[Dumper] The Thelia admin called '%s' performs a SQL data dump on the '%s' database at %s.",
            $admin->getLogin(),
            $this->dbName,
            date('r')
        ));
        
        return [$sql];
    }
    
    public function getFilename()
    {
        return $this->dbName.'_'.date('Ymd_His');
    }
}

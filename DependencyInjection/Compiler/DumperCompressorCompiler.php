<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Dumper\DependencyInjection\Compiler;

use Dumper\Dumper;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Description of DumperCompressorCompiler
 *
 * @author Romain
 */
class DumperCompressorCompiler implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(Dumper::DCM_SERVICE_ID)) {
            return;
        }
        
        $definition = $container->findDefinition(Dumper::DCM_SERVICE_ID);
        
        $taggedServices = $container->findTaggedServiceIds(
            Dumper::DUMPER_FORMATTER_TAG
        );
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall(
                'addDumperCompressor',
                array(new Reference($id))
            );
        }
    }
}

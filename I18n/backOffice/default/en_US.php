<?php
return [
    'dump_form' => [
        'advanced params' => [
            'dump' => [
                'add-drop-database' => 'Add "DROP DATABASE" statements before "CREATE DATABASE" statements?',
                'add-drop-table' => 'Add "DROP TABLE" statements before "CREATE TABLE" statements?',
                'add-drop-trigger' => 'Add "DROP TRIGGER" statements before "CREATE TRIGGER" statements?',
                'add-locks' => 'Add locks to each table before its dump?',
                'compress' => [
                    'label' => 'Compress:',
                    'none' => 'No compression',
                ],
                'default-character-set' => [
                    'label' => 'Default character set',
                    'utf8' => 'utf8',
                    'utf8mb4' => 'utf8mb4',
                ],
                'disable-keys' => 'Disable keys in tables?',
                'exclude-tables' => 'Exclude the following tables from the dump:',
                'extended-insert' => 'Use the multiline INSERT syntax?',
                'hex-blob' => 'Dump binary columns using hexadecimal notation?',
                'include-tables' => 'Include the following tables in the dump:',
                'label' => 'Dump parameters',
                'lock-tables' => 'Lock all the tables before beginning the dump?',
                'no-autocommit' => 'Disable autocommit?',
                'no-create-info' => 'Do not write statements related to table creation ("CREATE TABLE")?',
                'no-data' => 'Exclude table contents from the dump?',
                'single-transaction' => 'Put the whole dump in a single transaction?',
                'skip-comments' => 'Skip comments?',
                'skip-dump-date' => 'Do not write the dump date into this one?',
                'skip-triggers' => 'Exclude triggers for each table in the output?',
                'skip-tz-utc' => 'Do not force the timezone to UTC?',
                'tables error %dbname %tables' => 'The following tables do not belong to the "%dbname" database: %tables.',
                'tables-multiselect' => [
                    'filterPlaceholder' => 'Search',
                    'nonSelectedText' => 'No table',
                    'nSelectedText' => 'tables selected',
                    'selectAllText' => 'All the tables',
                ],
                'where' => 'WHERE statement to the dump command:',
            ],
            'label' =>  'Advanced parameters',
            'pdo' => [
                'label' => 'PDO parameters',
            ],
        ],
        'export' => 'Export',
        'header' => 'Databases',
        'invalid_db %dbname' => 'The "%dbname" database is not a valid database',
    ],
];

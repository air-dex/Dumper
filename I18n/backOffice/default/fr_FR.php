<?php
return [
    'dump_form' => [
        'advanced params' => [
            'dump' => [
                'add-drop-database' => 'Ajouter des instructions "DROP DATABASE" avant les instructions "CREATE DATABASE" ?',
                'add-drop-table' => 'Ajouter des instructions "DROP TABLE" avant les instructions "CREATE TABLE" ?',
                'add-drop-trigger' => 'Ajouter des instructions "DROP TRIGGER" avant les instructions "CREATE TRIGGER" ?',
                'add-locks' => 'Ajouter des verrous à chaque table avant son export ?',
                'compress' => [
                    'label' => 'Compression :',
                    'none' => 'Aucune compression',
                ],
                'default-character-set' => [
                    'label' => 'Encodage par défaut',
                    'utf8' => 'utf8',
                    'utf8mb4' => 'utf8mb4',
                ],
                'disable-keys' => 'Désactiver les index pour les tables ?',
                'exclude-tables' => 'Exclure les tables suivantes du dump :',
                'extended-insert' => 'Utiliser la syntaxe multilignes de INSERT ?',
                'hex-blob' => 'Exporter les colonnes binaires avec une notation hexadécimale ?',
                'include-tables' => 'Inclure les tables suivantes dans le dump :',
                'label' => 'Paramètres du dump',
                'lock-tables' => 'Verrouiller toutes les tables avant de commencer le dump ?',
                'no-autocommit' => 'Désactiver l\'autocommit ?',
                'no-create-info' => 'Ne pas écrire les instructions de création de tables ("CREATE TABLE") ?',
                'no-data' => 'Exclure le contenu des tables du dump ?',
                'single-transaction' => 'Mettre les instructions dans un seul bloc de transaction ?',
                'skip-comments' => 'Ne pas écrire les commentaires ?',
                'skip-dump-date' => 'Ne pas écrire la date du dump dans celui-ci ?',
                'skip-triggers' => 'Exclure les triggers pour chaque table du dump ?',
                'skip-tz-utc' => 'Forcer le fuseau horaire sur UTC ?',
                'tables error %dbname %tables' => 'Les tables suivantes n\'appartiennent pas à la base de données "%dbname" : %tables.',
                'tables-multiselect' => [
                    'filterPlaceholder' => 'Rechercher',
                    'nonSelectedText' => 'Aucune table',
                    'nSelectedText' => 'tables sélectionnées',
                    'selectAllText' => 'Toutes les tables',
                ],
                'where' => 'Instruction WHERE pour le dump :',
            ],
            'label' =>  'Paramètres avancés',
            'pdo' => [
                'label' => 'Paramètres PDO',
            ],
        ],
        'export' => 'Exporter',
        'header' => 'Base de données',
        'invalid_db %dbname' => 'La base de données "%dbname" n\'est pas une base de données valide.',
    ],
];

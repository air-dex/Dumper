<?php

namespace Dumper\Tools;

use Symfony\Component\HttpKernel\Kernel;
use Thelia\Core\Thelia;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Hacked Thelia Kernel in order to retrieve Propel's config.
 *
 * @author Romain
 */
class Propeller extends Thelia
{
    public function __construct($env, $debug)
    {
        Kernel::__construct($env, $debug);
    }
    
    public function getPropelConfig()
    {
        return parent::getPropelConfig();
    }
}
